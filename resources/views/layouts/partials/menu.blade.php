<li><a href="{{ $menu['path'] }}">{{ $menu['title'] }}</a>
@if (count($menu['submenu']) > 0)
    <ul>
    @foreach($menu['submenu'] as $menu)
	@include('layouts.partials.menu', $menu)
    @endforeach
    </ul>
@endif</li>

