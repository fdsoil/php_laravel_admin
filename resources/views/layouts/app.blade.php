<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">


<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
 

    <link href="{{ asset('css/menu.css') }}" rel="stylesheet">
    <link href="{{ asset('css/grid.css') }}" rel="stylesheet">

</head>
<body>
    <div id="app">

        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" 
                            class="navbar-toggle collapsed" 
                            data-toggle="collapse" 
                            data-target="#app-navbar-collapse" 
                            aria-expanded="false">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <!--a class="navbar-brand" href="{{ url('/') }}">
                        {{ config('app.name', 'Laravel') }}
                    </a-->
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">                        
                        <!-- Authentication Links -->
                        @guest
                            <li><a href="{{ route('login') }}">Login</a></li>
                            <li><a href="{{ route('register') }}">Register</a></li>
                        @else
                            @each('layouts.partials.menu', session('menu'), 'menu', 'layouts.partials.menus-none')
                            <li class="dropdown">
                                <a href="#" 
                                   class="dropdown-toggle" 
                                   data-toggle="dropdown" 
                                   role="button" 
                                   aria-expanded="false" 
                                   aria-haspopup="true" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu">
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>


        @yield('content')
    </div>
    <!-- Scripts -->
    <!--script src="https://cdnjs.cloudflare.com/ajax/libs/vue/1.0.26/vue.min.js"></script-->
    <!--script src="https://cdnjs.cloudflare.com/ajax/libs/vue/1.0.26/vue.js"></script-->
    <script src="{{ asset('js/vue.js') }}"></script>
    <script src="{{ asset('js/axios.js') }}"></script>
    <!--script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.2/axios.min.js"></script-->
    <!--script src="{{ asset('js/app.js') }}"></script-->
    @yield('scripts')
    <!--script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script-->
    @include('sweet::alert')


<script>

var menuStart = () =>
{
    var aElements = document.querySelectorAll(".nav li>a[href='#']");
    var addEventListenerInGroup = (aObj, aEvent, oFun) => 
    {
        for ( var i = 0; i < aObj.length; i++ )
            for ( var j = 0; j < aEvent.length; j++ ){
                aObj[i].addEventListener( aEvent[j], oFun);
                }
    }
    addEventListenerInGroup(aElements, ["click"], function(event = window.event) {  myFunc(this)  } );
}

var getSiblings = (elem, tag = '') =>
{
    var siblings = [];
    var sibling = elem.parentNode.firstChild;
    var verdadero = () => {
        var resp = false;
        if (tag == '' || sibling.tagName === tag)
            resp = true;
        return resp;
    }
    while (sibling) {
        //if (sibling.nodeType === 1 && sibling !== elem && verdadero())
        if (sibling.nodeType === 0 && sibling !== elem && verdadero())
            siblings.push(sibling);
        sibling = sibling.nextSibling;
    }
    return siblings;
}

var findParentBySelector = (elm, selector) =>
{
    var collectionHas = (a, b) =>
    {
        for(var i = 0, len = a.length; i < len; i ++)
            if(a[i] == b) return true;        
        return false;
    }
    var all = document.querySelectorAll(selector);
    var cur = elm.parentNode;
    while(cur && !collectionHas(all, cur)) 
        cur = cur.parentNode; 
    return cur; 
}

var myFunc = (esto) => 
{ 
    var liUlStyleDisplayNone = (aObj = aSiblingsLi) =>
    {
        var elementUl;
        for(var i = 0, len = aObj.length; i < len; i ++){
            elementUl = aSiblingsLi[i].getElementsByTagName("ul");
            if (elementUl.length > 0)
                elementUl[0].style.display='none';
        }
    }

    var removeClass = (aObj, clase) =>
    {
        for(var i = 0, len = aObj.length; i < len; i ++)
            aObj[i].classList.remove(clase);       
    }

    var findLiRemoveClass = (element, clase) =>
    {
        var aObj = element.getElementsByTagName("li");
        removeClass(aObj);
    } 

    var findUlStyleDisplayNone = (element) =>
    {
        var aObj = element.getElementsByTagName("ul");
        for(let i = 0, len = aObj.length; i < len; i ++)
            aObj[i].style.display='none';
        
    }

    var liFindLiRemoveClass = (clase, aObj = aSiblingsLi) =>
    {
        for(let i = 0, len = aObj.length; i < len; i ++)
            findLiRemoveClass(aObj[i]);
    }

    var liFindUlStyleDisplayNone = (aObj = aSiblingsLi) =>
    {
        for(let i = 0, len = aObj.length; i < len; i ++) 
            findUlStyleDisplayNone(aObj[i]);        
    }

    esto.removeAttribute('href');
    var element = findParentBySelector( esto, 'li'); 
    if (element.classList.contains('open')) {
          element.classList.remove('open');
          findLiRemoveClass( element, 'open');
          findUlStyleDisplayNone( element );
    } else {
        element.classList.add("open");
        var aUlElementChildren = element.getElementsByTagName("ul");
        aUlElementChildren[0].style.display='block';
        var aSiblingsLi = getSiblings(element, 'LI');
        liUlStyleDisplayNone();
        removeClass(aSiblingsLi, "open");
        liFindLiRemoveClass("open");
        liFindUlStyleDisplayNone();
    }
}

    menuStart();




</script>

</body>
</html>

