var baseData = [
    { name: 'Chuck Norris0', power: Infinity, action: 'A1' },
    { name: 'Bruce Lee0',  power: 9000, action: 'A2' },
    { name: 'Jackie Chan0', power: 7000, action: 'A3' },
    { name: 'Jet Li0', power: 8000 ,action: 'A4'},

    { name: 'Chuck Norris1', power: Infinity, action: 'B1' },
    { name: 'Bruce Lee1',  power: 9000, action: 'B2'},
    { name: 'Jackie Chan1', power: 7000, action: 'B3' },
    { name: 'Jet Li1', power: 8000, action: 'B4' },

    { name: 'Chuck Norris2', power: Infinity, action: 'C1' },
    { name: 'Bruce Lee2',  power: 9000, action: 'C2' },
    { name: 'Jackie Chan2', power: 7000, action: 'C3' },
    { name: 'Jet Li2', power: 8000, action: 'C4' },

    { name: 'Chuck Norris3', power: Infinity, action: 'D1' },
    { name: 'Bruce Lee3',  power: 9000, action: 'D2' },
    { name: 'Jackie Chan3', power: 7000, action: 'D3' },
    { name: 'Jet Li3', power: 8000, action: 'D4' },

    { name: 'Chuck Norris4', power: Infinity, action: 'E1' },
    { name: 'Bruce Lee4',  power: 9000, action: 'E2' },
    { name: 'Jackie Chan4', power: 7000, action: 'E3' },
    { name: 'Jet Li4', power: 8000, action: 'E4' },

    { name: 'Chuck Norris5', power: Infinity, action: 'F1' },
    { name: 'Bruce Lee5',  power: 9000, action: 'F2' },
    { name: 'Jackie Chan5', power: 7000, action: 'F3' },
    { name: 'Jet Li5', power: 8000, action: 'F4' },

    { name: 'Chuck Norris6', power: Infinity, action: 'G1' },
    { name: 'Bruce Lee6',  power: 9000, action: 'G2' },
    { name: 'Jackie Chan6', power: 7000, action: 'G3' },
    { name: 'Jet Li6', power: 8000, action: 'G4' },

    { name: 'Chuck Norris7', power: Infinity, action: 'H1' },
    { name: 'Bruce Lee7',  power: 9000, action: 'H2' },
    { name: 'Jackie Chan7', power: 7000, action: 'H3' },
    { name: 'Jet Li7', power: 8000, action: 'H4' },

    { name: 'Chuck Norris8', power: Infinity, action: 'I1' },
    { name: 'Bruce Lee8',  power: 9000, action: 'I2' },
    { name: 'Jackie Chan8', power: 7000, action: 'I3' },
    { name: 'Jet Li8', power: 8000, action: 'I4' },

    { name: 'Chuck Norris9', power: Infinity, action: 'J1' },
    { name: 'Bruce Lee9',  power: 9000, action: 'J2' },
    { name: 'Jackie Chan9', power: 7000, action: 'J3' },
    { name: 'Jet Li9', power: 8000, action: 'J4' },

    { name: 'Chuck Norris10', power: Infinity, action: 'K1' },
    { name: 'Bruce Lee10',  power: 9000, action: 'K2' },
    { name: 'Jackie Chan10', power: 7000, action: 'K3' },
    { name: 'Jet Li10', power: 8000, action: 'K4' },

    { name: 'Chuck Norris11', power: Infinity, action: 'L1' },
    { name: 'Bruce Lee11',  power: 9000, action: 'L2' },
    { name: 'Jackie Chan11', power: 7000, action: 'L3' },
    { name: 'Jet Li11', power: 8000, action: 'L4' },

    { name: 'Chuck Norris12', power: Infinity, action: 'M1' },
    { name: 'Bruce Lee12',  power: 9000, action: 'M2' },
    { name: 'Jackie Chan12', power: 7000, action: 'M3' },
    { name: 'Jet Li12', power: 8000, action: 'M4' }
];


