var updateModel = new Vue({
    el: "#edit", 
    computed: {
        parents: function () {
            let arr = this.menuOption.familia.split(' / ');
            this.nivel = arr.length -1;
            this.title = arr.pop();
            this.menuOption.title = this.title;
            return arr;
        }
    },            
    data: {
        menuOption: {'id': '', 'familia': '', 'path': '', 'sort': '', 'title': ''},
        title: '',
        nivel: 0,
        id: null,
    },
    methods: {
        updateMenu: function () {
            let url = 'http://localhost/php_laravel_admin/public/menu';
            axios.put(url, this.menuOption).then(response => {
               gridViewModel.getMenus();
               this.menuOption= {'id': '', 'familia': '', 'path': '', 'sort': '', 'title': ''};
               this.errors = [];
               $('#edit').modal('hide');
               alert('Opción actualizada con éxito');
               //toastr.success('Tarea actualizada con éxito');
            }).catch(error => {
               this.errors = error.response.data;
            });
        }
    }    
});



