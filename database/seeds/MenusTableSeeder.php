<?php

use Illuminate\Database\Seeder;
use App\Models\Menu;

class MenusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    { 
        Menu::create(["title" => "Google"                  , "parent_id" =>  0, "path" => "https://www.google.co.ve/"               , "sort" => 1]);
        Menu::create(["title" => "PHP"                     , "parent_id" =>  0, "path" => "#"                                       , "sort" => 2]);
        Menu::create(["title" =>"JavaSript"                , "parent_id" =>  0, "path" => "#"                                       , "sort" => 3]);
        Menu::create(["title" =>"Data Base"                , "parent_id" =>  0, "path"=> "#"                                        , "sort" => 4]);
        Menu::create(["title" =>"Zend"                     , "parent_id" =>  2, "path"=> "https://framework.zend.com/"              , "sort" => 1]);
        Menu::create(["title" =>"Synfony"                  , "parent_id" =>  2, "path"=> "http://symfony.es/"                       , "sort" => 2]);
        Menu::create(["title" =>"Larave"                   , "parent_id" =>  2, "path"=> "http://laravel.com"                       , "sort" => 3]);
        Menu::create(["title" =>"Reat"                     , "parent_id" =>  3, "path"=> "#"                                        , "sort" => 1]);
        Menu::create(["title" =>"Vue"                      , "parent_id" =>  3, "path"=> "#"                                        , "sort" => 2]);
        Menu::create(["title" =>"PostgreSQL"               , "parent_id" =>  4, "path"=> "#"                                        , "sort" => 1]);
        Menu::create(["title" =>"Wikipedia - Reat"         , "parent_id" =>  8, "path"=> "https://es.wikipedia.org/wiki/React"      , "sort" => 1]);
        Menu::create(["title" =>"WebSite - Reat"           , "parent_id" =>  8, "path"=> "https://es.reactjs.org/"                  , "sort" => 2]);
        Menu::create(["title" =>"GitHub - Reat"            , "parent_id" =>  8, "path"=> "https://github.com/facebook/react"        , "sort" => 3]);
        Menu::create(["title" =>"Wikipedia - Vue"          , "parent_id" =>  9, "path"=> "https://en.wikipedia.org/wiki/Vue.js"     , "sort" => 1]);
        Menu::create(["title" =>"WebSite - Vue"            , "parent_id" =>  9, "path"=> "https://vuejs.org/"                       , "sort" => 2]);
        Menu::create(["title" =>"GitHub - Vue"             , "parent_id" =>  9, "path"=> "https://github.com/vuejs"                 , "sort" => 3]);
        Menu::create(["title" =>"Ver 9.6"                  , "parent_id" => 10, "path"=> "#"                                        , "sort" => 1]);
        Menu::create(["title" =>"Wikipedia - PostgreSQL...", "parent_id" => 17, "path"=> "https://es.wikipedia.org/wiki/PostgreSQL" , "sort" => 1]);
        Menu::create(["title" =>"WebSite - PostgreSQL..."  , "parent_id" => 17, "path"=> "https://postgresql.org.es"                , "sort" => 2]);
        Menu::create(["title" =>"GitHub - PostgreSQL..."   , "parent_id" => 17, "path"=> "https://github.com/postgres/postgres"     , "sort" => 3]);
        Menu::create(["title" =>"Administration"           , "parent_id" =>  0, "path"=> "#"                                        , "sort" => 99]);
        Menu::create(["title" =>"Menu..."                  , "parent_id" =>  21, "path"=> "menu/"                                   , "sort" => 1]);
    }
}

