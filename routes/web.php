<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/menu', 'Admin\MenuController@index')->name('menu');

Route::get('/menu/create', 'Admin\MenuController@create')->name('menu/create');

Route::get('/menu/edit/{id}', 'Admin\MenuController@edit')->name('menu/edit/id');

Route::get('/menu/get/{id}', 'Admin\MenuController@get');

Route::get('/menu/recursive/', 'Admin\MenuController@recursive');

Route::get('/menu/children/{id}', 'Admin\MenuController@children');

Route::post('/menu/store', 'Admin\MenuController@store');

Route::put('/menu', 'Admin\MenuController@update');

Route::delete('/menu/{id}', 'Admin\MenuController@destroy');

Route::get('/role', 'Admin\RoleController@index')->name('role');

//Route::get('/role', 'Admin\RoleController@get');

Route::get('tipo/{type}', 'SweetController@notification');


